#ifndef __TIMER_H__
#define __TIMER_H__

#include "stdio.h"
#include "stdlib.h"
#include <time.h>

struct timespec tic();
struct timespec toc(struct timespec t1, float *tdif);

#endif