#include <stdlib.h>
#include <stdio.h>
#include "realtime.h"
#include "USBcom.h"
#include "timer.h"
#include "iotxt.h"
#include "FKIK.h"

#define MY_PRIORITY (98) /* we use 49 as the PRREMPT_RT use 50
                            as the priority of kernel tasklets
                            and interrupt handler by default */
#define MAX_SAFE_STACK (1024*1024) /* The maximum stack size which is
                                   guaranteed safe to access without
                                   faulting */
#define TxSize 64

#define RxSize 32

#define NANODELAY 400000

#define TRYNO 10000   //No. of data tried.

#define BAUDRATE 6000000

void dumpbuf(unsigned char *buf, int bufSize){
	for (int i=0; i<bufSize; i++){
		printf("%d ", buf[i]);
	}
	printf("\n");
}

/* 
	main
*/

int main()
{
	
	/* Variable for Tx Rx */
	unsigned char Txbuf[TxSize];
	unsigned char Rxbuf[RxSize];
	struct timespec t1, t2, ts;
	
	/* Variable for FK IK */
	double deg[6] = {0,0,0,0,0,0}; // Starting with[0, -90, 0, 0, 0, 0]
	double coord[6];    
	double deg2[48]; // 8x6 IK should have maximum of 8 solutions
	
	/* Variable for time log */
	float dump[TRYNO];
	float res[TRYNO];

	/* Open and setup the USB port */
	int device = 0;
	UCHAR latency_ms = 1; // set the latency timer to 0ms
	DWORD InTransferSize = 64; // set the USB in put transfer size to 64 bytes
	FT_HANDLE ft = openUSBserial(device, latency_ms, InTransferSize);
	
	/* set the read and write speed */
	setBAUDRATE(ft, BAUDRATE);

	/* Enable RTS/CTS flow control */
	//Handshaking
	fcRTSCTS(ft, 1); // 1 enable , 0 disable
	
	/* Make the process becomes realtime */    
	make_realtime(getpid(), MY_PRIORITY);
	stack_prefault(MAX_SAFE_STACK);


	/* Set value into Txbuf */
	for (int i=0; i< TxSize; i++){
		Txbuf[i] = i;
	}
	
	int j=0;
	//DWORD WordSend =0;

	/* Read and Write 2 times */
	t1 = tic();
	setRTS(ft,0);
	//setDTR(ft,0);
	
	while(j <TRYNO){
		
		printf("No %d ... \n", j);
		for (int i=0; i< RxSize; i++){
			Rxbuf[i] = 85;
		}
		/* read buf */
		USBRead(ft, Rxbuf, RxSize);
		
		dumpbuf(Rxbuf, RxSize);
		
		
		for (int i=0; i<RxSize; i++){
			Txbuf[i] = Rxbuf[i];
		}
		//dumpbuf(Txbuf, TxSize);
		/* FK IK */ 
  		t2 = tic();
		FK_Result(deg,coord); //FK
		IK_Result(coord,deg2); //IK
		t2 = toc(t2, &dump[j]);
		
		/* send buf */
		setRTS(ft,1);
		clock_gettime(CLOCK_MONOTONIC, &ts);
		ts.tv_nsec+=NANODELAY;
		tsnorm(&ts);
		
		USBWrite(ft, Txbuf, TxSize);
		//printf("Word Send = %d!\n", WordSend);
		//nanosleep(NANODELAY);
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts, NULL);
		setRTS(ft,0);
		
		t1 = toc(t1, &res[j]);

		j++;
	}


	/* close the serial port */
	closeUSBserial(ft);

	iotxt(res, TRYNO, "time_TxRx.txt");
	iotxt(dump, TRYNO, "time_FKIK.txt");

	return 0;
}
