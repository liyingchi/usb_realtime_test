#include "USBcom.h"
/*
FLAG of FT_STATUS

	FT_OK = 0
	FT_INVALID_HANDLE,
	FT_DEVICE_NOT_FOUND
	FT_DEVICE_NOT_OPENED,
	FT_IO_ERROR,
	FT_INSUFFICIENT_RESOURCES,
	FT_INVALID_PARAMETER,
	FT_INVALID_BAUD_RATE,

	FT_DEVICE_NOT_OPENED_FOR_ERASE,
	FT_DEVICE_NOT_OPENED_FOR_WRITE,
	FT_FAILED_TO_WRITE_DEVICE,
	FT_EEPROM_READ_FAILED,
	FT_EEPROM_WRITE_FAILED,
	FT_EEPROM_ERASE_FAILED,
	FT_EEPROM_NOT_PRESENT,
	FT_EEPROM_NOT_PROGRAMMED,
	FT_INVALID_ARGS,
	FT_NOT_SUPPORTED,
	FT_OTHER_ERROR,
	FT_DEVICE_LIST_NOT_READY,
*/
FT_STATUS ft_status;

/*
	Close the port 
*/
void closeUSBserial(FT_HANDLE ft){
	ft_status = FT_Close(ft);
	if(ft_status != FT_OK){
		printf("closeUSBserial(), ft_status = %d!\n", ft_status);
		exit(1);
	}
}


/*
	Open the USB serial port and set it up
*/

FT_HANDLE openUSBserial(int device, UCHAR latency_ms, DWORD InTransferSize){

	FT_HANDLE ft;
		
	ft_status = FT_Open(device, &ft);
	if (ft_status !=FT_OK){
		printf("Can not open device! ft_status = %d.\n Please check if module \"ftdi_sio\" is removed!\n", ft_status);
		exit(1);
	}
		
	ft_status = FT_Purge(ft, FT_PURGE_RX | FT_PURGE_TX);
	if (ft_status !=FT_OK){
		printf("Can not purge device! ft_status = %d.\n", ft_status);
		exit(1);
	}
		
	ft_status = FT_SetLatencyTimer(ft, latency_ms);
	if (ft_status !=FT_OK){
		printf("Can not set latency timer to %d! ft_status = %d.\n",latency_ms, ft_status);
		exit(1);
	}
	
	ft_status = FT_SetUSBParameters(ft, InTransferSize, 0); // Set input and output cache/buffer size (input:64, output:0 ???)
	if (ft_status !=FT_OK){
		printf("Can not set USB In transfer size %d bytes, must be multiple if 64 bytes! ft_status = %d.\n",InTransferSize, ft_status);
		exit(1);
	}
	
	ft_status = FT_SetDataCharacteristics(ft, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_NONE);
	if (ft_status !=FT_OK){
		printf("Can not set data into 8N1! ft_status = %d.\n", ft_status);
		exit(1);
	}
		
	return ft;
}

/* 
	Open serial port in raw mode, with custom baudrate if necessary 
*/
void setBAUDRATE(FT_HANDLE ft, DWORD baudrate){

	ft_status = FT_SetBaudRate(ft, baudrate);
	if (ft_status !=FT_OK){
		printf("Set baudrate to %.2fMbps failed! ft_status = %d!\n", (float)baudrate/1000000, (int)ft_status);
		exit(1);
	}
		
}


void fcRTSCTS(FT_HANDLE ft, int en){
	if (en){
		ft_status = FT_SetFlowControl(ft, FT_FLOW_RTS_CTS, 0x11, 0x13);
		if (ft_status != FT_OK){
			printf("Can not enable Hardware flow control RTS/CTS! ft_status = %d!\n", ft_status);
			exit(1);
		}
	}
	else {
		ft_status = FT_SetFlowControl(ft, FT_FLOW_NONE, 0x11, 0x13);
		if (ft_status != FT_OK){
			printf("Can not disable Hardware flow control RTS/CTS! ft_status = %d!\n", ft_status);
			exit(1);
		}
	}
}

void fcDTRDSR(FT_HANDLE ft, int en){
	if (en){
		ft_status = FT_SetFlowControl(ft, FT_FLOW_DTR_DSR, 0x11, 0x13);
		if (ft_status != FT_OK){
			printf("Can not enable Hardware flow control DTR/DSR! ft_status = %d!\n", ft_status);
			exit(1);
		}
	}
	else {
		ft_status = FT_SetFlowControl(ft, FT_FLOW_NONE, 0x11, 0x13);
		if (ft_status != FT_OK){
			printf("Can not disable Hardware flow control DTR/DSR! ft_status = %d!\n", ft_status);
			exit(1);
		}
	}
}

void setRTS(FT_HANDLE ft, int level){

	if (level){
		ft_status = FT_SetRts(ft);
			if (ft_status != FT_OK){
				printf("Set RTS to LOW falied, ft_status = %d!\n", ft_status);
				exit(1);
			}
	}
	else{
		ft_status = FT_ClrRts(ft);
			if (ft_status != FT_OK){
				printf("Set RTS to High falied, ft_status = %d!\n", ft_status);
				exit(1);
			}
	}
}


void setDTR(FT_HANDLE ft, int level){

	if (level){
		ft_status = FT_SetDtr(ft);
			if (ft_status != FT_OK){
				printf("Set DTR to LOW falied, ft_status = %d!\n", ft_status);
				exit(1);
			}
	}
	else{
		ft_status = FT_ClrDtr(ft);
			if (ft_status != FT_OK){
				printf("Set DTR to High falied, ft_status = %d!\n", ft_status);
				exit(1);
			}
	}
}


DWORD USBWrite(FT_HANDLE ft, unsigned char *Txbuf, DWORD TxSize){
	DWORD ByteWritten;
	ft_status = FT_Write(ft, Txbuf, TxSize, &ByteWritten);
	
	if ( (ft_status !=FT_OK) || (ByteWritten != TxSize) ){
		printf("Write failed, Want to write %d, but only %d bytes are written.\n ft_status = %d!\n", TxSize, ByteWritten, ft_status);
		exit(1);
	}
	
	return ByteWritten;
}

DWORD USBRead(FT_HANDLE ft, unsigned char *Rxbuf, DWORD RxSize){

	DWORD ByteReceived = 0;
	ft_status = FT_Read(ft, Rxbuf, RxSize, &ByteReceived);
	
	if (ft_status !=FT_OK){
		printf("Read failed. ft_status = %d!\n", ft_status);
		exit(1);
	}
	else {
		if (ByteReceived != RxSize){
			printf("Timeout in USBRead(): Data requested is %d bytes, but you get only %d bytes!", RxSize, ByteReceived);
			exit(1);
		}
	}

	return ByteReceived;
}
	
	
/*
	combine 4 char to 1 float 
*/ 

void uchar2float(unsigned char *cbuf, float *fbuf, int bufno){
 union {unsigned char b[4]; float f; } x;

  for (int i=0; i<bufno/4; i++){
    x.b[0] = cbuf[4*i];
    x.b[1] = cbuf[4*i+1];
    x.b[2] = cbuf[4*i+2];
    x.b[3] = cbuf[4*i+3]; 
    fbuf[i] = x.f;
  }
}

/*
	seperate 1 float to 4 char
*/ 
void float2uchar(unsigned char *cbuf, float *fbuf, int bufno){
  union {float f; unsigned char b[4]; } x;

  for (int i=0; i<bufno/4; i++){
    x.f = fbuf[i];
    cbuf[4*i] = x.b[0];
    cbuf[4*i+1] = x.b[1];
    cbuf[4*i+2] = x.b[2];
    cbuf[4*i+3] = x.b[3];
  }
}
	
	
	
	
	
	
	
	
	


