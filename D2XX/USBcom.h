#ifndef __USBCOM_H__
#define __USBCOM_H__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "ftd2xx.h"

void closeUSBserial(FT_HANDLE ft);
FT_HANDLE openUSBserial(int device, UCHAR latency_ms, DWORD InTransferSize);
void setBAUDRATE(FT_HANDLE ft, DWORD baudrate);
void fcRTSCTS(FT_HANDLE ft, int en);
void fcDTRDSR(FT_HANDLE ft, int en);
void setRTS(FT_HANDLE ft, int level);
void setDTR(FT_HANDLE ft, int level);
DWORD USBWrite(FT_HANDLE ft, unsigned char *Txbuf, DWORD TxSize);
DWORD USBRead(FT_HANDLE ft, unsigned char *Rxbuf, DWORD RxSize);

void uchar2float(unsigned char *cbuf, float *fbuf, int bufno);
void float2uchar(unsigned char *cbuf, float *fbuf, int bufno);

#endif
