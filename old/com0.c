#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h> // needed for memset
#include <sched.h>
#include <sys/mman.h>
#include <time.h>
#include "set_baud.h"
#include "timer.h"
#include "iotxt.h"

#define MY_PRIORITY (49) /* we use 49 as the PRREMPT_RT use 50
                            as the priority of kernel tasklets
                            and interrupt handler by default */

#define MAX_SAFE_STACK (8*1024) /* The maximum stack size which is
                                   guaranteed safe to access without
                                   faulting */

#define bufSize 15

#define TRYNO 10000   //No. of data tried.



/* Real time function for RT-Preempt*/
typedef void (*func_ptr) (void);

void make_realtime (void)
{
  struct sched_param param = { MY_PRIORITY };
  printf("getpid () = %d\n", getpid ());
  if (sched_setscheduler (getpid (), SCHED_FIFO, &param) < 0)
    {
      perror ("could not get real-time priority");
      exit (1);
    }
  if (mlockall (MCL_CURRENT | MCL_FUTURE) < 0)
    {
      perror ("could not lock process in memory");
      exit (2);
    }
}

/* Stack prefault function */ 
void stack_prefault(void) {

        unsigned char dummy[MAX_SAFE_STACK];

        memset(dummy, 0, MAX_SAFE_STACK);
}


int main()
{

// create the struct
struct termios options;
const char *device = "/dev/ttyUSB0";

int fd = open(device,  O_RDWR | O_NOCTTY | O_NDELAY );  
if(fd == -1) {
	printf("Can not open port %s!", device);
}

// set the read and write speed
// All speeds can be prefixed with B as a settings up to B1500000
// Or Enter your Own baud rate up to 12,000,000.
int sb = set_baud(fd, 6000000);
if (sb !=0 ) {
  printf("Set baud failed!\n");
  return -1;
}

// get the current settings of the serial port
tcgetattr(fd, &options);
tcflush(fd, TCIOFLUSH);

// PARENB is enable parity bit so this disables the parity bit
options.c_cflag &= ~PARENB;
// CSTOPB means 2 stop bits otherwise (in this case) only one stop bit
options.c_cflag &= ~CSTOPB;
// CSIZE is a mask for all the data size bits, so anding with the negation clears out the current data size setting
options.c_cflag &= ~CSIZE;
// CS8 means 8-bits per work
options.c_cflag |= CS8;
options.c_cc[VMIN] = 1;
options.c_cflag |= (CLOCAL | CREAD);

if(tcsetattr(fd, TCSANOW, &options)!= 0) {
   printf("Seting attribute of serial port fail!\n");

}


/* Make the process becomes realtime */    
make_realtime();
stack_prefault();


char Txbuf[bufSize];
char Rxbuf[bufSize];

struct timespec t1;
float res[TRYNO];

/* Set value into Txbuf */
int i=0;
for (i=0; i< bufSize; i++){
	Txbuf[i] = i;
}

int wordsRead = -1;
int j=0;


/* Read and Write 2 times */
while(j <TRYNO){

  printf("No %d, Time Used ... \n", j);
  t1 = tic();
  /* Read the data from Rx pin */
	while(1) {
		wordsRead = read(fd, Rxbuf, bufSize);
		if (wordsRead>0) break;
	}
  //printf("No %d: Time for Read ...", j);
  //t1 = toc(t1);

  /* Print out the received data */
	//printf("Read = %d, Complete reading!\n",wordsRead);

	// for (i=0; i<bufSize; i++){
	// 	printf("buf[%d] = %d\n", i, Rxbuf[i]);
	// }
  //t1 = tic();
  /* Write the data on Txbuf to Tx pin */
	int wordsWritten = write(fd, Txbuf, bufSize);
	tcdrain(fd); // wait until writing complete
  //printf("Time for Write ...");
  t1 = toc(t1, &res[j]);
  printf("\n");
	printf("write %d data!\n", wordsWritten);

	j++;
}


// close the serial port
if(close(fd) == -1) {
	   printf("Fail to close the port!\n");
}

iotxt(res, TRYNO, "time_rt49_USB49.txt");

return 0;
}
