#include "USBcom.h"

static struct termios oldterminfo;

/* 
	define the standard Buadrate with integer
*/
static int rate_to_constant(int baudrate) {
#define B(x) case x: return B##x
	switch(baudrate) {
		B(50);     B(75);     B(110);    B(134);    B(150);
		B(200);    B(300);    B(600);    B(1200);   B(1800);
		B(2400);   B(4800);   B(9600);   B(19200);  B(38400);
		B(57600);  B(115200); B(230400); B(460800); B(500000); 
		B(576000); B(921600); B(1000000);B(1152000);B(1500000);
	default: return 0;
	}
#undef B
}    


/*
	Close the port 
*/
void closeUSBserial(int fd){
	tcsetattr(fd, TCSANOW, &oldterminfo);
	if(close(fd) < 0)
		perror("closeUSBserial()");
}


/*
	Open the USB serial port and set it up
*/

int openUSBserial(const char *devicename, int icanon_en){

	int fd;
	struct termios newterminfo;
	
	if ((fd =open(devicename, O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK)) == -1){
		perror("openUSBserial(): open()");
		exit(1);
	} 
	
	if (tcgetattr(fd, &oldterminfo) == -1) {
		perror("openUSBserial(): tcgetattr()");
		exit(1);
	}

	newterminfo = oldterminfo;
	
	/* 	
		CRTSCTS: 	output hardware flow control
		CS8: 		8n1 (8 bits, no parity, 1 stop bit)
		CLOCAL:  	local connectio, no modem control
		CREAD: 		enable receiving characters 
	*/
	//newterminfo.c_cflag |= CRTSCTS | CS8 | CLOCAL | CREAD;
	newterminfo.c_cflag |= CS8 | CLOCAL | CREAD;
	
	/* 
		Raw output 
	*/
	newterminfo.c_oflag = 0;
	
	
	if (icanon_en){
		/* Enable Canonical mode */
		newterminfo.c_lflag |= ICANON;
		/* Set to non canonical mode */
	}
	else{
		newterminfo.c_lflag &= ~ICANON;
		newterminfo.c_cc[VTIME] = 0;
		newterminfo.c_cc[VMIN] =16;
	}
	/* 
		clean the modem line and activate the setting for the port
	*/
	if (tcflush(fd, TCIFLUSH) == -1){
		perror("openUSBserial(): tcflush()");
		exit(1);
	}
	
	if (tcsetattr(fd, TCSANOW, &newterminfo) == -1){
		perror("openUSBserial(): tcsetattr()");
		exit(1);
	}
	
	return fd;
}



/* 
	Open serial port in raw mode, with custom baudrate if necessary 
*/
int setBAUDRATE(int fd, int rate)
{
	struct termios options;
	struct serial_struct serinfo;
	int speed = 0;

	speed = rate_to_constant(rate);
	printf("speed = %d\n",speed );
	if (speed == 0) {
		/* Custom divisor */
		serinfo.reserved_char[0] = 0;
		if (ioctl(fd, TIOCGSERIAL, &serinfo) < 0)
			return -1;
		serinfo.flags &= ~ASYNC_SPD_MASK;
		serinfo.flags |= ASYNC_SPD_CUST;
		serinfo.flags |= ASYNCB_LOW_LATENCY;
		serinfo.custom_divisor = (serinfo.baud_base + (rate / 2)) / rate;
		//serinfo.custom_divisor = serinfo.baud_base  / rate;
		if (serinfo.custom_divisor < 1) 
			serinfo.custom_divisor = 1;
		if (ioctl(fd, TIOCSSERIAL, &serinfo) < 0)
			return -1;
		if (ioctl(fd, TIOCGSERIAL, &serinfo) < 0)
			return -1;
		if (serinfo.custom_divisor * rate != serinfo.baud_base) {
			printf("actual baudrate is %d / %d = %f", serinfo.baud_base, serinfo.custom_divisor,
			      (float)serinfo.baud_base / serinfo.custom_divisor);
		}
		printf("baud_rate = %f\n", (float)serinfo.baud_base / serinfo.custom_divisor);
	}

	fcntl(fd, F_SETFL, 0);
	tcgetattr(fd, &options);
	cfsetispeed(&options, speed ?: B38400);
	cfsetospeed(&options, speed ?: B38400);
	if (tcsetattr(fd, TCSANOW, &options) != 0){
		perror("set BUADRATE fail!\n");
		return -1;
	}

	return 1;
}

int setRTS(int fd, int level){

	int status;
	
	if(ioctl(fd, TIOCMGET, &status) == -1){
		perror("setRTS(): TIOCMGET");
		return -1;
	}
	
	if (level)
		status |= TIOCM_RTS;
	else
		status &= ~TIOCM_RTS;
		
	if(ioctl(fd, TIOCMSET, &status) == -1){
		perror("setRTS(): IOCMSET");
		return -1;
	}
	return 1;
}

/* 
	Combine 4 char to 1 float
*/
void uchar2float(unsigned char *cbuf, float *fbuf, int bufsize){
  union {unsigned char b[4]; float f; } x;

  for (int i=0; i<bufsize/4; i++){
    x.b[0] = cbuf[4*i];
    x.b[1] = cbuf[4*i+1];
    x.b[2] = cbuf[4*i+2];
    x.b[3] = cbuf[4*i+3]; 
    fbuf[i] = x.f;
  }
}


void float2uchar(unsigned char *cbuf, float *fbuf, int bufsize){
  union {float f; unsigned char b[4]; } x;

  for (int i=0; i<bufsize/4; i++){
    x.f = fbuf[i];
    cbuf[4*i] = x.b[0];
    cbuf[4*i+1] = x.b[1];
    cbuf[4*i+2] = x.b[2];
    cbuf[4*i+3] = x.b[3];
  }
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


