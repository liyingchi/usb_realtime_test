#include "FKIK.h"

using std::cout;
using std::endl;
/******************
// All the matrices are represented by column-based 1d array;
// e.g Matrix M = [0 3 6]
//                [1 4 7]
//                [2 5 8]
// are save as A[9] = {0, 1, 2, 3, 4, 5, 6, 7, 8} 
// M(i,j) = A[i+rows*j], rows is the no. of rows in the matrix 
*******************/




/* customize the bals atrix routine */
void DGEMM(char transa, char transb, int m, int n, int k, double alpha, double *a, int lda, double *b, int ldb, double beta, double *c, int ldc ){
  dgemm_(&transa, &transb, &m, &n, &k, &alpha, a, &lda, b, &ldb, &beta, c, &ldc);
}

/*Find inverse of 3x3 matrix*/
void MINV3x3(double* A)
{
    int N=3, LWORK=9, IPIV[4], INFO;
    double WORK[9];

    dgetrf_(&N,&N,A,&N,IPIV,&INFO);
    dgetri_(&N,A,&N,IPIV,WORK,&LWORK,&INFO);
}


/*==================================================
robot_arm essential parameter
in
deg: 6 degree
DH:define a 6x4 matrix before call the funtion

out
DH: By the reference return the essential
     parameter DH for following calculation
===================================================*/
/* Initial the valuw of DH, size of DH = 6x4*/
const static void DH_init(double *DH){
DH[0]=0;    DH[6]= 0;   DH[12]=0; 
DH[1]=-90;  DH[7]=88;   DH[13]=0;  
DH[2]=0;    DH[8]=260;  DH[14]=0;  
DH[3]=-90;  DH[9]=80;   DH[15]=282; 
DH[4]=90;   DH[10]=0;   DH[16]=0;   
DH[5]=-90;  DH[11]=0;   DH[17]=0; 
}


/*DH for FK*/
void DH_Parameter_FK(double *deg, double *DH) 
{
    
  DH_init(DH);

  for(int i=0; i<6; i++)
  {    
    DH[i] = DH[i]/180*PI;       // DH(i,0) = DH[i]
    DH[i+18] = deg[i]/180*PI;   // DH(i,3) = DH[i+6*3]      
  }
   
}

/*=======================================================
DH: Same as above 
T_num : T_num T multiplication 
T(0-6) = T(0-1)*T(1-2)*T(2-3)*T(3-4)*T(4-5)*T(5-6)
return the T(0-6) transformation matrix
========================================================*/
void T_Final(double *T, double *DH, int T_num)
{

  double TS[16], Temp[16]; // 4x4 tempory matrix 

  /*Initial T as 4x4 Identity matrix */
  for (int i=0; i<16; i++) T[i] = 0;
  T[0] = 1; T[5]=1; T[10] = 1; T[15]=1;

   
  for(int i=0;i<T_num;i++)
  {
      
    TS[0]=cos(DH[i+6*3]); TS[4]=-sin(DH[i+6*3]); TS[8]=0; TS[12]=DH[i+6*1];
    TS[1]=sin(DH[i+6*3])*cos(DH[i]); TS[5]=cos(DH[i+6*3])*cos(DH[i]); TS[9]=-sin(DH[i]); TS[13]=-sin(DH[i])*DH[i+6*2];
    TS[2]=sin(DH[i+6*3])*sin(DH[i]); TS[6]=cos(DH[i+6*3])*sin(DH[i]); TS[10]=cos(DH[i]); TS[14]=cos(DH[i])*DH[i+6*2];
    TS[3]=0; TS[7]=0; TS[11]=0; TS[15]=1;   //transformation matrix

    /*Temp = T*Ts */
    DGEMM('N', 'N', 4, 4, 4, 1.0, T, 4, TS, 4, 0.0, Temp, 4);
    /*T = Temp*/ 
    for (int i=0; i<16; i++){
        T[i] = Temp[i];
    }

  }

  /* if value in T less than threshold (TOL), set to 0*/
  for (int i=0; i<16; i++){
      if(fabs(T[i])< TOL)
        T[i]=0;
  }

}

/*==============================================================
calculate the Euler Angle and get the FK resule
in
DH: Same as above
FK_T:  define a 4x4 matrix before call the T_Final funtion
coord: define a 6x1 matrix before call the FK_Result funtion 

out
coord : By the reference return the coordinates of robot_arm  
===============================================================*/
void FK_Result(double *deg, double *coord){
  double DH[24], FK_T[16];  // DH: 6x4, FK_T:4x4
  double v_2, UVW[3];       // UVW: 3x1

  DH_Parameter_FK(deg, DH);
  T_Final(FK_T, DH, 6);

  v_2 = sqrt(FK_T[0]*FK_T[0]+FK_T[1]*FK_T[1]);
  UVW[1] = atan2(-FK_T[2],v_2);
  if(fabs(UVW[1] - PI/2)<=TOL)
  {
    UVW[0] = 0;
    UVW[1] = PI/2;
    UVW[2] = atan2(FK_T[4], FK_T[5]);
  }
  else if(fabs(UVW[1] + PI/2)<=TOL)
  {
    UVW[0] = 0;
    UVW[1] = -PI/2;
    UVW[2] = -atan2(FK_T[4], FK_T[5]);
  }
  else{
  UVW[0] = atan2(FK_T[1]/cos(UVW[1]),FK_T[0]/cos(UVW[1]));    //FK[0] = FK(0,0)  FK[1] = FK(1,0) 
  UVW[2] = atan2(FK_T[6]/cos(UVW[1]),FK_T[10]/cos(UVW[1]));   //FK[6] = FK(2,1)  FK[10] = FK(2,2)
  }
   
  coord[0]= FK_T[12];   //FK[12] = FK(0,3)
  coord[1] = FK_T[13];  //FK[13] = FK(1,3)
  coord[2] = FK_T[14];  //FK[14] = FK(2,3)

  // 20140930 IMPORTANT: Below is changed to meet the result of FK_v2
  coord[3] = UVW[0]/PI*180;
  coord[4] = UVW[1]/PI*180;
  coord[5] = UVW[2]/PI*180;

}

void DH_Parameter_IK(double *DH)  //robot_arm essential parameter
{
  DH_init(DH);
  for(int i=0; i<6; i++)
    DH[i] = DH[i]/180*PI;
}
  


/*=================================================================
coord: coordinates
deg_3: By the reference return the deg_3
=================================================================*/
void radian_3(double *coord, double *deg_3, double *DH)
{

  //DH[0...5] = DH(0...5,0) 
  //DH[6...11] = DH(0...5,1)
  //DH[12...17] = DH(0...5,2)
  //DH[18...23] = DH(0...5,3)

  double r = coord[0]*coord[0]+coord[1]*coord[1]+coord[2]*coord[2];  //r = x^2+y^2+z^2
  double a1_p = DH[7]*DH[7];  //a1^2     
  double R = r - a1_p; //R=r-a1^2 
  double x_a = 2 * R + 4 * a1_p; //x^2-(2R+4a1^2)x+4a^2*Z^2+R^2=0; divide equation
  double x_b = sqrt(x_a*x_a - 4 * (4 * a1_p * coord[2]*coord[2] + R*R));
  double x1 = (x_a + x_b)/2;
  double x2 = (x_a - x_b)/2;

  double p_1 = 2 * DH[8] * DH[15] * sin(DH[3]); //2*a2*d4*sin(alpha3)
  double p_2 = 2 * DH[8] * DH[9]; //2*a2*a3
  double p_3 = DH[9]*DH[9] + DH[15]*DH[15] + DH[14]*DH[14] + DH[8]*DH[8]; //a3^2+d4^2+d3^2+a2^2
  double u1_a = x1 - p_3 + p_2;
  double u1_b = -2 * p_1;
  double u1_c = x1 - p_3 - p_2;
  double u1 = (-u1_b + sqrt(u1_b*u1_b - 4 * u1_a * u1_c)) / (u1_a*2); 
  double u2 = (-u1_b - sqrt(u1_b*u1_b - 4 * u1_a * u1_c)) / (u1_a*2); 
  deg_3[0] = 2 * atan(u1);
  deg_3[1] = 2 * atan(u2);
   
  double u2_a = x2 - p_3 + p_2;
  double u2_b = -2 * p_1;
  double u2_c = x2 - p_3 - p_2;
  double u3 = (-u2_b + sqrt(u2_b*u2_b - 4 * u2_a * u2_c)) / (u2_a*2); 
  double u4 = (-u2_b - sqrt(u2_b*u2_b - 4 * u2_a * u2_c)) / (u2_a*2);
  deg_3[2] = 2 * atan(u3);
  deg_3[3] = 2 * atan(u4);

  
  for (int i = 0; i < 4; i++)
    {
      if (fabs(deg_3[i])<TOL)
        deg_3[i] = 0;
    }

}  

/*=================================================================
coord: coordinates
deg_3: By the reference return the deg_2
=================================================================*/
void radian_2(double *coord, double *deg_3, double *deg_2, double *DH)
{

  double r, k1, k2, k3, a1, a2, b1, b2, c1, c2, cos_deg, sin_deg;

  for(int i=0;i<4;i++)
  {
    r = coord[0]*coord[0]+coord[1]*coord[1]+coord[2]*coord[2];  //r = x^2+y^2+z^2
    k1 = DH[9] * cos(deg_3[i]) - DH[15] * sin(deg_3[i]) + DH[8]; //k1 = a1*cos(radian_3) - d4*sin(radian_3)
    k2 = -DH[9] * sin(deg_3[i]) - DH[15] * cos(deg_3[i]); //k2 = -a1*sin(radian_3)-d4*cos(radian_3)
    k3 = k1*k1+k2*k2+DH[7]*DH[7];
    a1 = sin(DH[1])*k1;
    b1 = -sin(DH[1])*k2;
    c1 = -coord[2];
    a2 = 2*DH[7]*k2;
    b2 = 2*DH[7]*k1;
    c2 = k3-r;
    cos_deg = (c2*a1-c1*a2)/(a2*b1-a1*b2); 
    sin_deg = -(c1+b1*cos_deg)/a1; 
    deg_2[i] = atan2(sin_deg,cos_deg); 
  }

   for (int i = 0; i < 4; i++)
    {
      if (fabs(deg_2[i])<TOL)
        deg_2[i] = 0;
    }

}



/*=================================================================
coord: coordinates
deg_3: By the reference return the deg_1
=================================================================*/
void radian_1(double *coord, double *deg_3, double *deg_2, double *deg_1, double *DH)
{

  double k1, k2, g1, g2, cos_deg, sin_deg;

  for(int i=0;i<4;i++)
  {
    k1 = DH[9] * cos(deg_3[i]) - DH[15] * sin(deg_3[i]) + DH[8];
    k2 = -DH[9] * sin(deg_3[i]) - DH[15] * cos(deg_3[i]);
    g1 = k1 * cos(deg_2[i]) + k2 * sin(deg_2[i]) + DH[7];
    g2 = k1 * sin(deg_2[i]) * cos(DH[1]) - k2 * cos(deg_2[i])
           * cos(DH[1]) - DH[13] * sin(DH[1]);
    sin_deg =  (g1 * coord[1] - g2 * coord[0]) / (g1*g1 + g2*g2);   
    cos_deg = (g1 * coord[0] + g2 * coord[1]) /  (g1*g1 + g2*g2);   
    deg_1[i] = atan2(sin_deg,cos_deg);    
  }
  
  for (int i = 0; i < 4; i++)
    {
      if (fabs(deg_1[i])<TOL)
        deg_1[i] = 0;
    }
  
}



void R_ALL(double *coord, double *R0_6) {
  //coord:  6x1
  //R0_6 :  3x3
  //R0_6[0..2] = R0_6(0..2,0)
  //R0_6[3..5] = R0_6(0..2,1)
  //R0_6[6..8] = R0_6(0..2,2)
  
  R0_6[0]=cos(coord[3]) * cos(coord[4]);
  R0_6[3]=cos(coord[3]) * sin(coord[4]) *sin(coord[5]) - sin(coord[3]) * cos(coord[5]);
  R0_6[6]=cos(coord[3]) * sin(coord[4]) *cos(coord[5]) + sin(coord[3]) * sin(coord[5]);
  R0_6[1]=sin(coord[3]) * cos(coord[4]);
  R0_6[4]=sin(coord[3]) * sin(coord[4]) *sin(coord[5]) + cos(coord[3]) * cos(coord[5]);
  R0_6[7]=sin(coord[3]) * sin(coord[4]) *cos(coord[5]) - cos(coord[3]) * sin(coord[5]);
  R0_6[2]=-sin(coord[4]);
  R0_6[5]=cos(coord[4]) * sin(coord[5]);
  R0_6[8]=cos(coord[4]) * cos(coord[5]);
  

   for (int i = 0; i < 9; i++)
    {
      if (fabs(R0_6[i])<TOL)
        R0_6[i] = 0;
    }
}


void radian(double *coord,double *deg)
{

  //deg_all[0...7] = deg_all(0...7, 0)
  //deg_all[8...15] = deg_all(0...7, 1)
  //deg_all[16...7] = deg_all(0...7, 2)
  //deg_all[24...7] = deg_all(0...7, 3)
  //deg_all[32...7] = deg_all(0...7, 4)
  //deg_all[40...7] = deg_all(0...7, 5)

  double deg_1[4], deg_2[4], deg_3[4]; 
  double DH[24];
  double R0_6[9];     //3x3
  double T0_4[16];    //4x4
  double R0_4[9];     //3x3
  double R4_6[9];     //3x3
  double deg_all[48]; //8x6


  coord[3] = coord[3]/180*PI;
  coord[4] = coord[4]/180*PI;
  coord[5] = coord[5]/180*PI; // transform Input uvw to radian

  DH_Parameter_IK(DH);
  radian_3(coord, deg_3, DH);
  radian_2(coord, deg_3, deg_2, DH);
  radian_1(coord, deg_3, deg_2, deg_1, DH);
  R_ALL(coord, R0_6); //R(0-6) 

  for(int i=0; i<4;i++)
  {
      DH[18] = deg_1[i];
      DH[19] = deg_2[i];
      DH[20] = deg_3[i];
      DH[21] = 0;
      T_Final(T0_4,DH,4);

      for(int p=0; p<3; p++){
        for (int q=0; q<3; q++){
          R0_4[p+3*q] = T0_4[p+4*q];
        }
      }

      MINV3x3(R0_4);
      DGEMM('N', 'N', 3, 3, 3, 1.0, R0_4, 3, R0_6, 3, 0.0, R4_6, 3);
    
      // for (int k=0; k<9; k++){
      //   if(fabs(R4_6[k])<1e-10)
      //     R4_6[k] =0;
      // }

    // for (int p=0; p<3; p++){
    //   for (int k=0; k<3; k++){
    //     cout << R4_6[p+3*k] << " ";
    //   }
    //   cout << endl;
    // }
    // cout << endl;


      deg_all[i] = deg_1[i];
      deg_all[i+8*1] = deg_2[i];
      deg_all[i+8*2] = deg_3[i];
      deg_all[i+4] = deg_1[i];
      deg_all[i+4+8*1] = deg_2[i];
      deg_all[i+4+8*2] = deg_3[i];
      deg_all[i+8*4] = -atan2(sqrt(R4_6[2]*R4_6[2] + R4_6[5]*R4_6[5]), R4_6[8]);
      deg_all[i+4+8*4] = -atan2(-sqrt(R4_6[2]*R4_6[2] + R4_6[5]*R4_6[5]), R4_6[8]);
      if( fabs(deg_all[i+8*4])<= 1.0e-5 || fabs( deg_all[i+4+8*4] )<= 1.0e-5)
      {
        deg_all[i+8*3] = 0;
        deg_all[i+8*4] = 0;
        deg_all[i+8*5] = atan2(-R4_6[3], R4_6[0]);
        deg_all[i+4+8*3] = 0;
        deg_all[i+4+8*4] = 0;
        deg_all[i+4+8*5] = atan2(-R4_6[3], R4_6[0]);
      }
      else if(fabs(fabs(deg_all[i+8*4]) - PI)<= 1.0e-5 || fabs(fabs(deg_all[i+4+8*4]) - PI)<= 1.0e-5 )
      {
        deg_all[i+8*3] = 0;
        deg_all[i+8*4] = PI;
        deg_all[i+8*5] = atan2(R4_6[3], -R4_6[0]);
        deg_all[i+4+8*3] = 0;
        deg_all[i+4+8*4] = PI;
        deg_all[i+4+8*5] = atan2(R4_6[3], -R4_6[0]);
      }
      else
      {
        deg_all[i+8*3] = atan2(R4_6[7]/sin(-deg_all[i+8*4]), R4_6[6]/sin(-deg_all[i+8*4]));
        deg_all[i+8*5] = atan2(R4_6[5]/sin(-deg_all[i+8*4]), -R4_6[2]/sin(-deg_all[i+8*4]));
        deg_all[i+4+8*3] = atan2(R4_6[7]/sin(-deg_all[i+4+8*4]), R4_6[6]/sin(-deg_all[i+4+8*4]));
        deg_all[i+4+8*5] = atan2(R4_6[5]/sin(-deg_all[i+4+8*4]), -R4_6[2]/sin(-deg_all[i+4+8*4]));
      }
  }

  // for(i=0; i<8; i++)
  // {
  //   if(isnan(fabs(deg_all(i,1))))
  //     N = N - 1;
  // }
  int t=0;
  for(int i=0; i<8; i++) 
  {
    if(!isnan(fabs(deg_all[i+8])))
    {
      for(int j=0; j<6; j++)
      deg[t+8*j] = deg_all[i+8*j]*180/PI;
      t++;
    }                        
  }
//std::cout << "deg = \t" << std::endl;
//std::cout << deg << std::endl;


}