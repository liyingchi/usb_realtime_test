#include "realtime.h"


typedef void (*func_ptr) (void);

void make_realtime (int pid, int priority)
{

	printf("getpid () = %d\n", pid);
	
	struct sched_param param = { priority };
	if (sched_setscheduler (pid, SCHED_FIFO, &param) < 0){
		perror ("could not get real-time priority");
		exit (1);
	}
	
	if (mlockall (MCL_CURRENT | MCL_FUTURE) < 0){
		perror ("could not lock process in memory");
		exit (2);
    }
}

/* 
	Stack prefault function 
*/ 
void stack_prefault(int max_safe_stack) {

	unsigned char *dummy;
	dummy = (unsigned char*)malloc(max_safe_stack*sizeof(unsigned char));
	memset(dummy, 0, max_safe_stack);
}

/*
	tsnorm
*/
void tsnorm(struct timespec *ts){
	while (ts->tv_nsec >=NSEC_PER_SEC){
		ts->tv_nsec -= NSEC_PER_SEC;
		ts->tv_sec++;
	}
}


/* 
	Combine 4 char to 1 float
*/

void nanosleep(int nanocount){
		struct timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		ts.tv_nsec+=nanocount;
		tsnorm(&ts);
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts, NULL);
}

