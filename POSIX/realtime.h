#ifndef __REALTIME_H__
#define __REALTIME_H__

#include <stdlib.h>
#include <stdio.h>
#include <string.h> // needed for memset
#include <sched.h>
#include <sys/mman.h>
#include <time.h>

#define NSEC_PER_SEC 1000000000

void make_realtime (int pid, int priority);
void stack_prefault(int max_safe_stack) ;
void tsnorm(struct timespec *ts);
void nanosleep(int nanocount);


#endif
