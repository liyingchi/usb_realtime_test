#include "timer.h"


struct timespec tic(){
	struct timespec t1;
	clock_gettime(CLOCK_MONOTONIC, &t1);
	return t1;
}

struct timespec toc(struct timespec t1, float *tdif){

	struct timespec t2;
	clock_gettime(CLOCK_MONOTONIC, &t2);
	*tdif = ((float)t2.tv_sec-t1.tv_sec)+((float)t2.tv_nsec-t1.tv_nsec)/1E9;
	*tdif = *tdif*1000000;
 	printf("Elapsed time is %.7f us!\n", *tdif);
 	return t2;

}
