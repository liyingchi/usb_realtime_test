#ifndef __IOTXT_H__
#define __IOTXT_H__

#include <iostream>
#include "stdio.h"
#include "stdlib.h"
#include <fstream>
#include <string>


void iotxt(float *mat, const int rows, const char *filename);

#endif