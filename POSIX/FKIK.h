#ifndef __FKIK_H__
#define __FKIK_H__

#include <iostream>
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#define PI 3.141592653589793
#define FULL_POSITION 20000
#define TOL 1e-12	// tolerence to decide = 0 

/***** add -lblas -llapack -lgfortran to the linker, for the detail of parameter, check wiki lapack blas*******/
extern "C" {
// BLAS routine for matrix multiplication
void dgemm_(char *transa, char *transb, int *m, int *n, int *k, double *alpha, double *a, int *lda, double *b, int *ldb, double *beta, double *c, int *ldc );

// LAPACK routine LU decomoposition of a general matrix
void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);

// LAPACK routine generate inverse of a matrix given its LU decomposition
void dgetri_(int* N, double* A, int* lda, int* IPIV, double* WORK, int* lwork, int* INFO);
}
/*********************************************************/

void FK_Result(double *deg, double *coord); // FK API
void radian(double *coord,double *deg);	// IK API

#endif


