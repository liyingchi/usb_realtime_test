#include <stdlib.h>
#include <stdio.h>
#include "realtime.h"
#include <time.h>
#include "USBcom.h"
#include "timer.h"
#include "iotxt.h"
#include "FKIK.h"

#define MY_PRIORITY (98) /* we use 49 as the PRREMPT_RT use 50
                            as the priority of kernel tasklets
                            and interrupt handler by default */

#define MAX_SAFE_STACK (64*1024) /* The maximum stack size which is
                                   guaranteed safe to access without
                                   faulting */
#define TxSize 16

#define RxSize 16

#define TRYNO 1000   //No. of data tried.

#define BAUDRATE 6000000


/* 
	main
*/

int main()
{
	
	unsigned char Txbuf[TxSize];
	unsigned char Rxbuf[RxSize];
	//float fbuf[bufSize/4];
	//float fdeg2[bufSize/4];
	//double deg[6] = {0,0,0,0,0,0}; // Starting with[0, -90, 0, 0, 0, 0]
	//double coord[6];    
	//double deg2[48]; // 8x6 IK should have maximum of 8 solutions
	struct timespec t1, t2;
	float dump[TRYNO];
	float res[TRYNO];

	/*
		Open and setup the USB port 
	*/
	const char *device = "/dev/ttyUSB0";
	int canon_enable = 1;
	int fd = openUSBserial(device, canon_enable);
	/*
		set the read and write speed
		All speeds can be prefixed with B as a settings up to B1500000
		Or Enter your Own baud rate up to 12,000,000.
	*/
	setBAUDRATE(fd, BAUDRATE);


	/* 
		Make the process becomes realtime
	*/    
	make_realtime(getpid(), MY_PRIORITY);
	stack_prefault(MAX_SAFE_STACK);


	/* 
		Set value into Txbuf 
	*/
	for (int i=0; i< TxSize; i++){
		Txbuf[i] = 'A'+i;
	}
	//Txbuf[TxSize]='\n';
	//printf("%d\n", Txbuf[TxSize]);
	//int wordsRead = -1;
	int j=0;
	int wordSend=0;
	int wordRead=0;
 

	/* 
		Read and Write 2 times 
	*/
	t1 = tic();
	while(j <TRYNO){
		//clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t2, NULL);
		printf("No %d, Time Used ... \n", j);
 	
  		/* 
  			Read the data from Rx pin 
  		*/
		//while(1) {
			wordRead = read(fd, Rxbuf, RxSize);
			//if (wordsRead>0) break;
		//}
		
		printf("wordRead = %d!\n", wordRead);
		for (int i=0; i<RxSize; i++){
			printf("%d ", Rxbuf[i]);
		}
		printf("\n");	
		
		
		//nanosleep(10000);
		
		//setRTS(fd, 1);
		wordSend = write(fd, Txbuf, TxSize);
		printf("wordsend = %d!\n", wordSend);
		tcdrain(fd);
		
		//nanosleep(500000);
		//setRTS(fd, 0);
		//t2.tv_nsec += 500000;
		//tsnorm(&t2);
		//clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t2, NULL);
		//t2.tv_nsec += 100000000;
		//tsnorm(&t2);
		
		
		
		
	
		
		/* 
  			FK IK 
  		*/ 
  		//t2 = tic();
		//FK_Result(deg,coord); //FK
		//radian(coord,deg2); //IK
		//t2 = toc(t2, &dump[j]);

		//usleep(500);
		//write(fd, Txbuf, bufSize);
		//tcdrain(fd); // wait until writing complete
		t1 = toc(t1, &res[j]);

		j++;
	}


	/*
		close the serial port
	*/
	//sleep(1);
	closeUSBserial(fd);

	//iotxt(res, TRYNO, "time_TxRx.txt");
	//iotxt(dump, TRYNO, "time_FKIK.txt");

	return 0;
}
