#ifndef __USBCOM_H__
#define __USBCOM_H__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/serial.h>

void closeUSBserial(int fd );
int openUSBserial(const char *devicenamevoid, int icanon_en);
int setBAUDRATE(int fd, int rate);
int setRTS(int fd, int level);
int getCTS(int fd);
void uchar2float(unsigned char *cbuf, float *fbuf, int bufsize);
void float2uchar(unsigned char *cbuf, float *fbuf, int bufsize);

#endif
